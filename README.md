# @patchman/typed (WIP)

Facility for converting untyped JSON-like Dynamic objects into statically typed values.

**Warning:** THIS IS A WORK-IN−PROGRESS

## Exemple

```hx
var obj: Dynamic = { one: [1, 2, null, 3], two: [4, 5, 6, null] };

// Prints: {two => [4,5,6,null], one => [1,2,null,3]}
var result: Map<String, Array<Null<Int>>> = Typed.read(obj);
trace(result);

// Throws: [Error at $.one[2]: null isn't a valid integer,Error at $.two[3]: null isn't a valid integer]
var result: Map<String, Array<Int>> = Typed.read(obj);
```
