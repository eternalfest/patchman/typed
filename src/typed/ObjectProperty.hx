package typed;

abstract ObjectProperty(Dynamic) {
  
  private inline function new(prop: Dynamic /* String or Int */): Void {
    this = prop;
  }

  @:from
  public static inline function fromField(field: String): ObjectProperty {
    return new ObjectProperty(field);
  }

  @:from
  public static inline function fromIndex(idx: Int): ObjectProperty {
    return new ObjectProperty(idx);
  }

  public inline function asField(): Null<String> {
    return TypeUtils.isString(this) ? cast this : null;
  }

  public inline function asIndex(): Null<Int> {
    return TypeUtils.isInt(this) ? cast this : null;
  }
}
