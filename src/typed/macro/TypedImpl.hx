package typed.macro;

#if macro

import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;
import haxe.macro.TypeTools;

class TypedImpl {

  public static inline var DEFAULT_READERS_CLASS: String = "typed.TypedReaders";
  private static var INSTANCE: Null<TypedImpl> = null;

  private static dynamic function installReuseHandler(): Void {
    Context.onMacroContextReused(function() {
      INSTANCE = null;
      return true;
    });

    installReuseHandler = function() {};
  }

  public static function getInstance(): TypedImpl {
    installReuseHandler();
    if (INSTANCE == null) {
      INSTANCE = new TypedImpl();
      INSTANCE.init();
    }
    return INSTANCE;
  }

  private var readerFactory: ReaderFactory;

  private function new() {
    this.readerFactory = new ReaderFactory();
  }

  private function init() {
    var defaultReaders = MacroUtils.getType(DEFAULT_READERS_CLASS, Context.currentPos());
    this.readerFactory.registerReaders(defaultReaders, false);
  }

  public function createReaderFromContext(extractFromTypedReader: Bool): Expr {
    var ty = Context.getExpectedType();
    var pos = Context.currentPos();
    if (ty == null)
      Context.error("Missing expected type", pos);

    if (extractFromTypedReader) {
      ty = MacroUtils.extractTypedReader(ty);
      if (ty == null) {
        Context.error('Expected type must be typed.TypedReader<T>', pos);
      }
    }

    return readerFactory.createReader(ty, pos);
  }

  public function showAllRegisteredReaders(): ExprOf<Void> {
    Context.warning(readerFactory.getAllReadersString(), Context.currentPos());
    return macro null;
  }
}
#end
