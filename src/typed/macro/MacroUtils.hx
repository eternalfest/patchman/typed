package typed.macro;

#if macro
import haxe.macro.Context;
import haxe.macro.Type;
import haxe.macro.Expr;

class MacroUtils {

  /**
    Convert a `BaseType` path to an unique string.
  **/
  public static inline function typePathToString(path: BaseType): String {
    // Identically named types in different modules are a compilation
    // error, so this is enough to guarantee uniqueness.
    return haxe.macro.MacroStringTools.toDotPath(path.pack, path.name);
  }

  /**
    Try to extract a dotted path from an expression.
  **/
  public static function exprToPath(expr: Expr): Null<Array<String>> {
    var path = [];
    while (true) {
      switch (expr.expr) {
        case EConst(CIdent(field)):
          path.push(field);
          path.reverse();
          return path;
        case EField(sub, field):
          path.push(field);
          expr = sub;
        default: return null;
      }
    }
  }

  /**
    Test if the given `BaseType` is `Null<T>`.
  **/
  public static inline function isNullType(baseType: BaseType): Bool {
    return baseType.pack.length == 0 && baseType.module == "StdTypes" && baseType.name == "Null";
  }

  /**
    Like `Context.getType`, but using the given location if an error occurs.
  **/
  public static function getType(name: String, pos: Position): Type {
    try {
      return Context.getType(name);
    } catch (error: String) {
      return Context.error(error, pos);
    }
  }

  /**
    Like `Context.follow`, but doesn't follow `Null<T>` typedefs.
  **/
  public static function followNoNulls(type: Type): Type {
    while(true) {
      type = switch (type) {
        case TLazy(lazy): lazy();
        case TMono(_.get() => t) if (t != null): t;
        case TType(ref, _) if (!isNullType(ref.get())):
          Context.follow(type, true);
        default: break;
      };
    }
    return type;
  }

  /**
    Try to extract the `T` from a `typed.TypedReader<T>`.
  **/
  public static function extractTypedReader(type: Type): Null<Type> {
    // We do this "manually" because Context.unify will unify
    // Null<Null<T>> with Null<T>, which breaks TypedReader inference.
    switch (followNoNulls(type)) {
      // ReaderContext -> Dynamic -> Null<T>
      case TFun([argCtx, argDyn], ret) if (!argCtx.opt && !argDyn.opt):
        if (!followNoNulls(argDyn.t).match(TDynamic(null)))
          return null;

        switch (followNoNulls(argCtx.t)) {
          case TInst(_.get() => t, []) if (
            t.pack.length == 1
            && t.pack[0] == "typed" && t.name == "ReaderContext"
            && t.module == "typed.ReaderContext"
          ): null;
          default: return null;
        }

        return switch (followNoNulls(ret)) {
          case TType(ref, [param]) if (isNullType(ref.get())): param;
          default: null;
        }
      default: return null;
    }
  }
}
#end
