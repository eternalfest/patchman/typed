package typed.macro;

#if macro

import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;

import typed.macro.meta.ReaderFieldMeta;

class ReaderFieldDiscovery {

  public static function discoverOnNamedType(type: Type, restrictPatterns: Bool): Array<ReaderField> {
    var baseType: BaseType = switch (type) {
      case TInst(ref, _): ref.get();
      case TEnum(ref, _): ref.get();
      case TAbstract(ref, _): ref.get();
      default: return []; // Typedefs can't have static fields or metadata, so no need to check them
    }

    var meta = ReaderFieldMeta.parseAll(baseType.meta.get());
    
    var readers = meta.map(findReaderField);
    if (readers.length == 0) {
      var statics = getStaticFields(type);
      if (statics != null)
        readers = searchStaticFields(baseType, statics);
    }

    if (restrictPatterns) {
      var requiredName = MacroUtils.typePathToString(baseType);
      for (reader in readers) {
        if (!reader.getPattern().isNominalType(requiredName)) {
          Context.error('TypedReader ${reader.getName()} must read a $requiredName', reader.getPos());
        }
      }
    }

    return readers;
  }

  private static function searchStaticFields(type: BaseType, statics: Array<ClassField>): Array<ReaderField> {
    var path = type.pack.concat([type.name]);
    var readers = [];

    for (field in statics) {
      var meta = ReaderFieldMeta.parseWithPath(field.meta.get(), path, field.name);
      if (meta != null)
        readers.push(ReaderField.fromAnnotatedField(meta, field));
    }

    return readers;
  }

  private static function findReaderField(meta: ReaderFieldMeta): ReaderField {
    var pos = meta.path.pos;
    var path = meta.path.v;
    var fieldName = path.pop();
    var typeName = path.join(".");
    path.push(fieldName);

    var statics = getStaticFields(MacroUtils.getType(typeName, pos));

    if (statics != null) {
      for (field in statics) {
        if (field.name == fieldName) {
          var reader = ReaderField.fromAnnotatedField(meta, field);
          if (reader != null) return reader;
        }
      }
    }

    return Context.error("Path must name a @:typedReader static field of a class or abstract", pos);
  }

  private static function getStaticFields(type: Type): Null<Array<ClassField>> {
    return switch (type) {
      case TInst(ref, _): ref.get().statics.get();
      case TAbstract(ref, _):
        var impl = ref.get().impl;
        return impl == null ? null : impl.get().statics.get();
      default: null;
    };
  }
}
#end
