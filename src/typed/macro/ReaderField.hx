package typed.macro;

#if macro

import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;

import typed.macro.pattern.TypePattern;
import typed.macro.meta.ReaderFieldMeta;

/**
  A class field annotated with `@:typedReader`.

  This must be a public static field, and can be either:
  - a read-only variable;
  - a non-macro and non-dynamic method.

  Its type must be of the form `TypedReader<T1> -> TypedReader<T2> -> ... -> TypedReader<R>`,
  with `R` a type convertible to a `TypePattern` mentioning all argument types `T`.

  If the `singleton` flag is specified, the must be of type `TypedReader<R>` and `R` mustn't
  contain any type variables.
**/
class ReaderField {

  public static inline var METADATA_NAME: String = ":typedReader";

  private var pos: Position;
  private var fullPath: Array<String>;
  private var pattern: TypePattern;
  private var singleton: Bool;
  
  private function new(pattern: TypePattern, meta: ReaderFieldMeta) {
    this.pos = meta.path.pos;
    this.fullPath = meta.path.v;
    this.pattern = pattern;
    this.singleton = meta.singleton.v;
  }

  public inline function getName(): String {
    return this.fullPath.join(".");
  }

  public inline function getPos(): Position {
    return this.pos;
  }

  public function getReaderExpr(params: Array<Expr>, pos: Position): Expr {
    if (params.length != this.pattern.nbParameters()) {
      Context.error(
        'TypedReader ${getName()} expects ${this.pattern.nbParameters()} arguments, but ${params.length} were supplied',
        pos
      );
    }

    // We don't use macroStringTools.toFieldExpr to preserve positions.
    var path = null;
    for (part in this.fullPath) {
      var e = path == null ? EConst(CIdent(part)) : EField(path, part);
      path = { expr: e, pos: pos };
    }
    path = path == null ? (macro @:pos(pos) null) : path; // if fullPath is empty

    if (this.singleton)
      return path;

    return macro @:pos(pos) $e{path}($a{params});
  }

  public inline function getPattern(): TypePattern {
    return this.pattern;
  }

  public static function fromAnnotatedField(meta: ReaderFieldMeta, field: ClassField): ReaderField {
    validateFieldKind(field);
    var pattern = validateFieldType(field.type, field.pos, meta.singleton.v);
    return new ReaderField(pattern, meta);
  }

  private static function validateFieldKind(field: ClassField): Void {
    var isKindValid = field.isPublic && field.kind.match(
      FMethod(MethNormal | MethInline) |
      FVar(AccNormal | AccInline, AccNo | AccNever)
    );
    if (!isKindValid) {
      Context.error('@$METADATA_NAME can only be used on public methods or read-only vars', field.pos);
    }
  }

  private static function validateFieldType(type: Type, pos: Position, singleton: Bool): TypePattern {
    var func = extractTypesFromTypedReader(type, singleton);
    if (func == null)
      Context.error('@$METADATA_NAME field has invalid type', pos);

    var pattern = TypePattern.create(func.args, func.ret);
    var errors = pattern.getErrors();
    if (errors != null) {
      var msg = ['@$METADATA_NAME field has an invalid pattern:'];
      for (error in errors) msg.push("- " + error);
      Context.error(msg.join("\n"), pos);
    }

    return pattern;
  }

  private static function extractTypesFromTypedReader(type: Type, singleton: Bool): Null<{ args: Array<Type>, ret: Type }> {
    if (singleton) {
      var ret = MacroUtils.extractTypedReader(type);
      return ret == null ? null : { args: [], ret: ret };
    }

    switch (MacroUtils.followNoNulls(type)) {
      case TFun(args, ret):
        var tyRet = MacroUtils.extractTypedReader(ret);
        var tyArgs = [];
        if (tyRet == null) return null;
        for (arg in args) {
          if (arg.opt) return null;
          var arg = MacroUtils.extractTypedReader(arg.t);
          if (arg == null) return null;
          tyArgs.push(arg);
        }
        return { args: tyArgs, ret: tyRet };
      default: return null;
    }
  }
}
#end
