package typed.macro;

#if macro

import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;
import typed.macro.pattern.TypeMatcher;

using haxe.macro.TypeTools;

/**
  A factory to build `TypedReader<T>`s at compile-time for
  arbitrary types `T`.
**/
class ReaderFactory {
  private var readerMatcher: TypeMatcher<ReaderField>;
  private var discoveredTypes: Map<String, Void>;

  public function new() {
    this.readerMatcher = new TypeMatcher();
    this.discoveredTypes = new Map();

    var matcher: Null<typed.macro.pattern.TypeMatcher<Int>> = null;
  }

  public function getAllReadersString(): String {
    var readers = [for (r in readerMatcher) r];
    return makeReaderList(readers, readers.length + " registered TypedReader's:");
  }

  public function registerReader(reader: ReaderField): Void {
    var pos = Context.currentPos();
    switch (this.readerMatcher.addPattern(reader.getPattern(), reader)) {
      case Success: null;
      case InternalError(msg):
        Context.error('Couldn\'t register TypedReader ${reader.getName()}: $msg', pos);
    }
  }

  public function registerReaders(readerHolder: Type, restrictPatterns: Bool): Void {
    var baseType: BaseType = switch (readerHolder) {
      case TLazy(lazy):
        return registerReaders(lazy(), restrictPatterns);
      case TMono(_.get() => t) if (t != null):
        return registerReaders(t, restrictPatterns);
      case TInst(ref, _): ref.get();
      case TEnum(ref, _): ref.get();
      case TAbstract(ref, _): ref.get();
      default: return; // We only supports classes, enums, and abstracts.
    };
    var name = MacroUtils.typePathToString(baseType);

    // Only run the discovery once per type.
    if (this.discoveredTypes.exists(name))
      return;

    this.discoveredTypes.set(name, null);
    for (reader in ReaderFieldDiscovery.discoverOnNamedType(readerHolder, restrictPatterns)) {
      registerReader(reader);
    }
  }

  // TODO: add support for "partially applied" readers, like Array<T>, by
  // allowing the user to pass extra parameters for the unresolved type variables.
  public function createReader(type: Type, pos: Position): Expr {
    // Make sure the TypedReaders associated to this type are registered.
    registerReaders(type, true);

    var matches = this.readerMatcher.findMatching(type);
    var reader = switch (matches.length) {
      case 0: return Context.error("No TypedReader is applicable for the type " + type.toString(), pos);
      case 1: matches[0];
      default:
        var readers = [for (m in matches) m.value];
        var error = makeReaderList(readers, 'Multiple TypedReader\'s are applicable for the type ${type.toString()}:');
        return Context.error(error, pos);
    };

    // Recursively create TypedReaders for each extracted type parameter.
    var exprParams = [for (ptype in reader.params) createReader(ptype, pos)];

    return reader.value.getReaderExpr(exprParams, pos);
  }

  private static function makeReaderList(readers: Array<ReaderField>, header: String): String {
    var msg = [header];
    for (reader in readers) {
      var name = reader.getName();
      var pat = reader.getPattern().toString();
      msg.push('- $name ($pat)');
    }
    return msg.join("\n");
  }
}
#end
