package typed.macro.meta;

#if macro
import haxe.macro.Expr;
import typed.macro.meta.MetaParser;

class ReaderFieldMeta {

  public static inline var NAME: String = ":typedReader";

  public var singleton: MetaValue<Bool>;
  public var path: MetaValue<Array<String>>;

  private function new() {
  }

  private static function parseInner(parser: MetaParser, path: Null<Array<String>>): ReaderFieldMeta {
    var meta = new ReaderFieldMeta();
    meta.singleton = parser.getFlag("singleton");
    meta.path = if (path == null) {
      parser.getAttr("path", parseReaderPath);
    } else {
      { v: path, pos: parser.getPos() };
    }

    parser.validateNoUnknown();
    return meta;
  }

  public static function parseWithPath(meta: Metadata, path: Array<String>, name: String): Null<ReaderFieldMeta> {
    var parser = MetaParser.parseOne(NAME, meta);
    return parser == null ? null : parseInner(parser, path.concat([name]));
  }

  public static function parseAll(meta: Metadata): Array<ReaderFieldMeta> {
    var parsers = MetaParser.parseAll(NAME, meta);
    return parsers.map(function(p) return parseInner(p, null));
  }

  private static function parseReaderPath(expr: Expr, error: MetaRaiseError): Array<String> {
    var path = MacroUtils.exprToPath(expr);
    if (path == null)
      error("Invalid path value", expr.pos);
    return path;
  }
}

#end