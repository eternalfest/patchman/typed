package typed.macro.meta;

#if macro
import haxe.macro.Context;
import haxe.macro.Expr;

typedef MetaRaiseError = String -> Null<Position> -> Dynamic;
typedef MetaAttrParser<T> = Expr -> MetaRaiseError -> T;
typedef MetaValue<T> = { v: T, pos: Position };

class MetaParser {

  private var name: String;
  private var pos: Position;
  private var attrs: Map<String, Array<MetaAttribute>>;
  private var unusedAttrs: Map<String, Position>;

  private function new(name: String, pos: Position) {
    this.name = name;
    this.pos = pos;
    this.attrs = new Map();
    this.unusedAttrs = new Map();
  }

  public inline function getPos(): Position {
    return this.pos;
  }

  public function addAttr(name: String, pos: Position, value: Null<Expr>): Void {
    var attr = this.attrs.get(name);
    if (attr == null) {
      attr = [];
      this.attrs.set(name, attr);
      this.unusedAttrs.set(name, pos);
    }

    attr.push({ namePos: pos, expr: value });
  }

  private function raiseError(error: String, pos: Null<Position>): Dynamic {
    return Context.error('@${this.name}: $error', pos == null ? this.pos : pos);
  }

  public function validateNoUnknown(): Void {
    for (name in this.unusedAttrs.keys()) {
      raiseError('Unknown attribute $name', this.unusedAttrs.get(name));
    }
  }

  public function getFlag(name: String): MetaValue<Bool> {
    this.unusedAttrs.remove(name);
    var values = this.attrs.get(name);

    if (values == null || values.length == 0)
      return { v: false, pos: this.pos };

    if (values.length == 1) {
      var val = values[0];
      if (val.expr != null)
        raiseError('Attribute $name doesn\'t take a value', val.expr.pos);
      return { v: true, pos: val.namePos };
    }

    return raiseError('Duplicate attribute $name', values[1].namePos);
  }

  public function getAttr<T>(name: String, parser: MetaAttrParser<T>): MetaValue<T> {
    var val = getOptAttr(name, parser);
    if (val.v == null)
      raiseError('Missing required attribute $name', val.pos);
    return val;
  }

  public function getOptAttr<T>(name: String, parser: MetaAttrParser<T>): MetaValue<Null<T>> {
    this.unusedAttrs.remove(name);
    var values = this.attrs.get(name);

    if (values == null || values.length == 0)
      return { v: null, pos: this.pos };

    if (values.length == 1) {
      var val = values[0];
      if (val.expr == null)
        raiseError('Attribute $name expects a value', val.expr.pos);
      return { v: parser(val.expr, this.raiseError), pos: val.namePos };
    }

    return raiseError('Duplicate attribute $name', values[1].namePos);
  }

  public function getAttrList<T>(name: String, parser: MetaAttrParser<T>): Array<MetaValue<T>> {
    this.unusedAttrs.remove(name);
    var values = this.attrs.get(name);

    if (values == null) return [];

    return [for (val in values) {
      if (val.expr == null)
        raiseError('Attribute $name expects a value', val.namePos);
      else { v: parser(val.expr, this.raiseError), pos: val.expr.pos };
    }];
  }

  private function parseMetadataEntry(meta: MetadataEntry): Void {
    if (meta.params == null) return;

    for (param in meta.params) {
      switch (param.expr) {
        case EConst(CIdent(name)):
          addAttr(name, param.pos, null);
        case EBinop(OpAssign, { expr: EConst(CIdent(name)), pos: pos }, value):
          addAttr(name, pos, value);
        default: raiseError('Invalid parameter (must be `name` or `name=value`)', param.pos);
      }
    }
  }

  public static function parseOne(name: String, metadata: Metadata): Null<MetaParser> {
    var parsed = null;

    for (meta in metadata) {
      if (meta.name != name) continue;
      if (parsed == null) {
        parsed = new MetaParser(name, meta.pos);
      } else {
        Context.error('Duplicate @$name metadata', meta.pos);
      }
      parsed.parseMetadataEntry(meta);
    }

    return parsed;
  }

  public static function parseAll(name: String, metadata: Metadata): Array<MetaParser> {
    var parsed = [];
    for (meta in metadata) {
      if (meta.name != name) continue;
      var p = new MetaParser(name, meta.pos);
      p.parseMetadataEntry(meta);
      parsed.push(p);
    }

    return parsed;
  }

  public static function parseMerged(name: String, metadata: Metadata): Null<MetaParser> {
    var parsed = null;

    for (meta in metadata) {
      if (meta.name != name) continue;
      if (parsed == null) parsed = new MetaParser(name, meta.pos);
      parsed.parseMetadataEntry(meta);
    }

    return parsed;
  }
}

private typedef MetaAttribute = {
  namePos: Position,
  expr: Null<Expr>,
};
#end