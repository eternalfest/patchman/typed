package typed.macro.pattern;

#if macro
import haxe.ds.Option;
import haxe.macro.Type;
import haxe.macro.Context;
import typed.macro.pattern.TypePattern;

using haxe.EnumTools;
using haxe.macro.TypeTools;

typedef TypeMatcherResult<V> = {
  value: V,
  params: Array<Type>,
};

enum TypeMatcherStatus {
  Success;
  InternalError(msg: String);
}

/**
  A mapping from `TypePattern`s to arbitrary values.
  A type may match several patterns, in which cases all
  corresponding values will be returned.
**/
class TypeMatcher<V> {

  private var values: Array<V>;
  private var patterns: Array<TypePattern>;
  private var matcher: InnerTypeMatcher;

  public function new() {
    this.values = [];
    this.patterns = [];
    this.matcher = new InnerTypeMatcher(true);
  }

  public inline function iterator(): Iterator<V> {
    return this.values.iterator();
  }

  /**
    Returns values (with their applied parameters) whose pattern match the given type.
    The applied parameters correspond to the type variables of the matching pattern.

    Only the most-specialized patterns are returned: for example, if`Array<T>` and
    `Array<String>` match, only `Array<String>` is returned.

    More formally, for two patterns `P` and `Q`, we say that "`P` specializes `Q`"
    (written `P <= Q`) if and only if:
     - `Q: Wildcard`; or
     - `P: Nominal(n, _), Q: Nominal(m, _)` and `n` is a typedef of `m`; or
     - `P: Nominal(n, p), Q: Nominal(n, q)` and `pi <= qi` for all `i`s.

    This is a pre-order, so we can derive the "strictly specializes" relation
    (written `P < Q`) as `(P <= Q) and not (Q <= P)`.

    Then, the pattern `P` is returned by this function if and only if:
      - `P` matches `type`;
      - there is no `Q` matching `type` such that `Q < P`.
  **/
  public function findMatching(type: Type): Array<TypeMatcherResult<V>> {
    var candidates = this.matcher.findAllMatching(type);
    var map = [for (match in candidates) match.id => match];

    // Remove from the map all candidates which are strictly specialized by another.
    for (match in candidates) {
      if (!map.exists(match.id)) continue;
      for (id in match.specializes) {
        var id = id.id;
        var spec = map.get(id);
        if (spec == null) continue;

        if (!spec.specializes.equals(match.specializes))
          map.remove(id);
      }
    }

    return [for (match in map) {
      value: this.values[match.id],
      params: this.patterns[match.id].reorderParams(match.params),
    }];
  }

  /**
    Add the given pattern (with the given value) to this matcher.
  **/
  public function addPattern(pattern: TypePattern, value: V): TypeMatcherStatus {
    var entries = [];
    try {
      this.matcher.locateEntries(pattern.kind, entries);
    } catch (error: PatternError) {
      return InternalError(error.toString());
    }

    var idx = this.values.length;
    this.values.push(value);
    this.patterns.push(pattern);
    for (e in entries) e.insert(idx);
    return Success;
  }
}

private typedef InnerMatch = {
  id: Int, // Id of the match.
  params: Array<Type>, // Parameters extracted by this match.
  specializes: ImmSet<{ id: Int }>, // List of matches specialized by this match. 
};

// The actual type matcher: maps type patterns to `Int` indices.
// To ensure that patterns stay distinguishable, each index should only be used once.
private class InnerTypeMatcher {
  // Possible values for nominal types (or type constructors).
  private var nominal: Null<Map<String, TypeMapEntry>>;
  // Possible values for the fully unrestricted type.
  private var wildcard: Null<TypeMapEntry>;

  // Indicates if matches reported by this matcher will always bubble up to the root matcher.
  private var matchesAreAlwaysValid: Bool;

  public function new(matchesAreAlwaysValid: Bool) {
    this.nominal = null;
    this.wildcard = null;
    this.matchesAreAlwaysValid = matchesAreAlwaysValid;
  }

  // Locate all the entries which must be modified to add the given pattern.
  // If an error occurs, a PatternError is thrown.
  public function locateEntries(pattern: TypePatternKind, entries: Array<TypeMapEntry>): Void {
    return switch (pattern) {
      case Nominal(name, params):
        if (this.nominal == null)
          this.nominal = new Map();
    
        var entry = this.nominal.get(name);
        if (entry == null) {
          entry = new TypeMapEntry(params.length);
          this.nominal.set(name, entry);
        }
        
        entry.locateEntries(params, entries);
      case Wildcard:
        if (this.wildcard == null)
          this.wildcard = new TypeMapEntry(0);
        this.wildcard.locateEntries([], entries);
    };
  }

  // Returns all match ids (with their applied parameters) whose pattern match the given type.
  public function findAllMatching(type: Type): ImmSet<InnerMatch> {
    // We can return only the most specific matches if we know that they are always valid.
    if (this.matchesAreAlwaysValid) {
      var result = ImmSet.empty();
      iterMatchesBySpecificity(type, function(matches) {
        result = matches;
        return true;
      });
      return result;
    }

    var bySpecificity = null;
    iterMatchesBySpecificity(type, function(matches) {
      if (bySpecificity == null) {
        bySpecificity = [matches];
      } else {
        bySpecificity.push(matches);
      }
      return false;
    });
    if (bySpecificity == null) return ImmSet.empty();

    // Collect all the matches into a single set, recording specialization info as we go.
    var matches = bySpecificity.pop();
    while(bySpecificity.length > 0) {
      var curMatches = bySpecificity.pop().map(function(m) {
        return {
          id: m.id,
          params: m.params,
          specializes: m.specializes.union(matches.downcast()),
        };
      });

      matches = matches.disjointUnion(curMatches);
    }

    return matches;
  }

  // Get all the matches for the given type, ordered from the most specific to the most generic.
  // Stops processing matches when `handler` return `true`.
  private function iterMatchesBySpecificity(type: Type, handler: ImmSet<InnerMatch> -> Bool): Void {
    // Nominal patterns are the most specific, with typedefs coming before their definitions.
    if (this.nominal != null) {
      var info = getNominalTypeInfo(type);
      while (info != null) {
        var entry = this.nominal.get(MacroUtils.typePathToString(info.path));
        var matches = entry == null ? ImmSet.empty() : entry.findAllMatching(info.params);
        if (matches.size() > 0 && handler(matches)) return;
        info = info.tdef == null ? null : getNominalTypeInfo(info.tdef);
      }
    }

    // The wildcard pattern is the most generic, and introduces a type parameter.
    if (this.wildcard != null) {
      var matches = this.wildcard.findAllMatching([]).map(function(m) {
        var params = m.params.copy();
        params.push(type);
        return { id: m.id, specializes: m.specializes, params: params };
      });

      if (matches.size() > 0) handler(matches);
    }
  }

  // If type is a nominal type (class, enum, abstract, typedef), get its info, else return null.
  // - path: the actual name of the nominal type
  // - params: the type parameters the nominal type was instanciated with
  // - tdef: if it was a typedef, the referenced type
  private function getNominalTypeInfo(type: Type): Null<{ path: BaseType, params: Array<Type>, tdef: Null<Type> }> {
    return switch (type) {
      case TLazy(lazy): getNominalTypeInfo(lazy());
      case TMono(ref):
        var t = ref.get();
        t == null ? null : getNominalTypeInfo(t);
      case TInst(ref, params): switch (ref.get().kind) {
        case KNormal: { path: ref.get(), params: params, tdef: null };
        default: null;
      }
      case TEnum(ref, params):
        { path: ref.get(), params: params, tdef: null };
      case TAbstract(ref, params):
        { path: ref.get(), params: params, tdef: null };
      case TType(ref, params):
        var t = ref.get();
        // Don't follow Null<T> typedefs.
        var tdef = MacroUtils.isNullType(t) ? null : Context.follow(type, true);
        { path: t, params: params, tdef: tdef };
      case TFun(_, _) | TDynamic(_) | TAnonymous(_):
        null;
    };
  }
}

// An entry for a given type, or type constructor.
private class TypeMapEntry {

  // The possible values, if no type parameters.
  // Should always be empty if params.length > 0.
  private var values: Array<InnerMatch>;
  // Submaps for each type parameter.
  // Each type parameter is independent; all maps must
  // give the same value id for the match to be valid.
  private var params: Array<InnerTypeMatcher>;

  public function new(nbParams: Int): Void {
    this.values = [];
    var matchesAreAlwaysValid = nbParams < 2;
    this.params = [for (_ in 0...nbParams) new InnerTypeMatcher(matchesAreAlwaysValid)];
  }

  public inline function insert(value: Int): Void {
    this.values.push({
      id: value,
      params: [],
      specializes: ImmSet.fromSorted(this.values).downcast(),
    });
  }

  public function locateEntries(params: Array<TypePatternKind>, entries: Array<TypeMapEntry>): Void {
    var len = this.params.length;
    if (params.length != len) {
      throw new PatternError('Wrong number of type parameters for type (expected $len; got ${params.length})');
    }

    if (len == 0) {
      entries.push(this);
    } else {
      for (i in 0...len) {
        this.params[i].locateEntries(params[i], entries);
      }
    }
  }

  public function findAllMatching(params: Array<Type>): ImmSet<InnerMatch> {
    var len = this.params.length;
    if (params.length != len) {
      return ImmSet.empty();
    } else if (len == 0) {
      return ImmSet.fromSorted(this.values);
    }

    var matches = this.params[0].findAllMatching(params[0]);

    for (i in 1...len) {
      if (matches.size() == 0) return matches; // early short-circuit if no possible match
      matches = combineMatches(matches, this.params[i].findAllMatching(params[i]));
    }
    return matches;
  }

  private static function combineMatches(lefts: ImmSet<InnerMatch>, rights: ImmSet<InnerMatch>): ImmSet<InnerMatch> {
    return lefts.intersection(rights, function(left, right) {
      return {
        id: left.id,
        params: left.params.concat(right.params),
        specializes: left.specializes.intersection(right.specializes),
      };
    });
  }
}

// An read-only set backed by a sorted array.
@:forward(iterator, join, toString)
private abstract ImmSet<T: { id: Int }>(Array<T>) to Iterable<T> {

  private static var EMPTY: Dynamic = [];
  public static inline function empty<T>(): ImmSet<T> {
    return EMPTY;
  }

  public static inline function fromSorted<T>(array: Array<T>): ImmSet<T> {
    return cast array;
  }

  public static inline function downcast<T, U: T>(set: ImmSet<U>): ImmSet<T> {
    return cast set; // safe, because ImmSet is immutable
  }

  public inline function size() {
    return this.length;
  }

  // Compute the union of ``this` and `other`, throwing an error if the swo sets aren't disjoint.
  public inline function disjointUnion(other: ImmSet<T>): ImmSet<T> {
    return union(other, combinerError);
  }

  // Compute the union of `this` and `other`, using `combiner` to merge equal elements.
  // Note: `combiner` must preserve the `id` field.
  public function union(other: ImmSet<T>, ?combiner: T -> T -> T): ImmSet<T> {
    var array = [];
    var other: Array<T> = cast other;
    var i = 0, j = 0, l1 = this.length, l2 = other.length;
    if (l1 == 0) return fromSorted(other);
    if (l2 == 0) return fromSorted(this);
    
    combiner = combiner == null ? combinerChooseLeft : combiner;
    do {
      var delta = this[i].id - other[j].id;
      if (delta == 0) {
        array.push(combiner(this[i++], other[j++]));
      } else if (delta < 0) {
        array.push(this[i++]);
      } else {
        array.push(other[j++]);
      }
    } while (i < l1 && j < l2);

    while (i < l1) array.push(this[i++]);
    while (j < l2) array.push(other[j++]);
    return fromSorted(array);
  }

  // Compute the intersection of `this` and `other`, using `combiner` to merge elements.
  // Note: `combiner` must preserve the `id` field.
  public function intersection(other: ImmSet<T>, ?combiner: T -> T -> T): ImmSet<T> {
    var array = null;
    var self: Array<T> = cast this;
    var other: Array<T> = cast other;
    var i = 0, j = 0, l1 = self.length, l2 = other.length;
    if (l1 == 0 || l2 == 0) return empty();

    combiner = combiner == null ? combinerChooseLeft : combiner;
    do {
      var delta = self[i].id - other[j].id;
      if (delta == 0) {
        var result = combiner(self[i++], other[j++]);
        if (array == null) {
          array = [result];
        } else {
          array.push(result);
        }
      } else if (delta < 0) {
        i++;
      } else {
        j++;
      }
    } while (i < l1 && j < l2);

    return array == null ? empty() : fromSorted(array);
  }

  // Maps the element of this set.
  // Note: `map` must preserving the `id` field.
  public function map<U: { id: Int }>(map: T -> U): ImmSet<U> {
    if (this.length == 0) return empty();
    return fromSorted(this.map(map));
  }

  // Test for set equality.
  public function equals(other: ImmSet<T>): Bool {
    var other: Array<T> = cast other;
    if(this.length != other.length) return false;
    for (i in 0...this.length) {
      if (this[i].id != other[i].id) return false;
    }
    return true;
  }

  private static function combinerChooseLeft<T>(a: T, b: T): T {
    return a;
  }
  
  private static function combinerError<T: { id: Int }>(a: T, b: T): T {
    throw new PatternError('Unexpected duplicate elements #${a.id} and #${b.id}');
  }

}
#end
