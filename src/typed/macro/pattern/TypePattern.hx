package typed.macro.pattern;

#if macro
import haxe.macro.Type;
import haxe.macro.MacroStringTools;

using haxe.EnumTools;
using haxe.macro.TypeTools;

enum TypePatternKind {
  /**
    Matches a nominal type (class, enum, abstract, typedef), with the given type parameters.
  **/
  Nominal(path: String, params: Array<TypePatternKind>);
  /**
    Matches any type and introduces a new unconstrained variable.
  **/
  Wildcard;
}

/**
  A 'shape' that can be matched by a single type, or a family of types,
  using a subset of the unification rules of the Haxe type system.
**/
class TypePattern {
  // The mapping from internal Wildcard order to external variable order.
  private var typeParamsList: Array<Int>;

  // If the pattern is invalid, is `null`.
  @:allow(typed.macro.pattern.TypeMatcher)
  private var kind: Null<TypePatternKind>;

  // The errors encountered when constructing this pattern. Empty if `kind` isn't `null`.
  private var errors: Array<String>;

  private function new(typeParamsList: Array<Int>, kind: Null<TypePatternKind>, errors: Array<String>) {
    this.typeParamsList = typeParamsList;
    this.kind = kind;
    this.errors = errors;
  }

  /**
    Create a `TypePattern` corresponding to the given type, parametrized
    by the given type variables. Each type variable must be a `TInst`
    instance with kind `KTypeParameter`.

    **Limitations:**
      - Type variables must be unconstrained.
      - Each type variable must be referenced exactly once.
      - `TInst` with a kind different of `KNormal` or `KTypeParameter` aren't supported.
      - `TFun`, `TAnonymous`, `TDynamic` aren't supported.
      - Typedefs (`TType`) aren't followed, and are treated as a nominal type.
  **/
  public static function create(params: Array<Type>, type: Type): TypePattern {
    var ctx = new CreateCtx();
    
    for (i in 0...params.length) ctx.addTypeParam(params[i], i);
    var kind: Null<TypePatternKind> = ctx.createPattern(type);
    ctx.finish();

    kind = ctx.errors.length > 0 ? null : kind;
    return new TypePattern(ctx.paramList, kind, ctx.errors);
  }

  /**
    Return a string representation of this pattern.
  **/
  public function toString(): String {
    if (this.kind == null)
      return "<INVALID>";
    var ctx = new ToStringCtx();
    for (i in this.typeParamsList) ctx.addTypeParam(i);
    ctx.visit(this.kind);

    return ctx.toString();
  }

  /**
    Return `null` if this pattern is valid, else return a list of errors.
  **/
  public inline function getErrors(): Null<Array<String>> {
    return this.kind == null ? this.errors : null;
  }

  /**
    Return the number of parameters for this pattern.
  **/
  public inline function nbParameters(): Int {
    return this.typeParamsList.length;
  }

  /**
    Check if this pattern only matches the given type path.
  **/
  public function isNominalType(path: String): Bool {
    return switch (this.kind) {
      case Nominal(name, _) if (path == name): true;
      default: false;
    };
  }

  @:allow(typed.macro.pattern.TypeMatcher)
  public function reorderParams<T>(params: Array<T>): Array<T> {
    return [for (i in 0...params.length) {
      params[this.typeParamsList[i]];
    }];
  }
}

// For converting a TypePattern to a String
private class ToStringCtx {
  private var parts: Array<String>;
  private var varNames: Array<String>;
  private var isInFreeParams: Bool;
  private var nextVar: Int;

  public function new() {
    this.parts = [];
    this.varNames = [];
    this.isInFreeParams = false;
    this.nextVar = 0;
  }

  private static var LETTERS: String = "abcdefghijklmnopqrstuvxyz";
  private static function makeVarName(n: Int): String {
    var base = LETTERS.length;
    n += 1;
    var name = "";
    while (n >= base) {
      name = LETTERS.charAt(n % base) + name;
      n = Std.int(n / base);
    }
    return "'" + LETTERS.charAt(n - 1) + name;
  }

  public function addTypeParam(idx: Int): Void {
    if (!this.isInFreeParams) {
      this.isInFreeParams = true;
      this.parts.push("for<");
    } else {
      this.parts.push(", ");
    }

    this.parts.push(makeVarName(this.varNames.length));
    this.varNames.push(makeVarName(idx));
  }

  public function visit(kind: TypePatternKind): Void {
    if (this.isInFreeParams) {
      this.isInFreeParams = false;
      parts.push("> ");
    }

    switch (kind) {
      case Nominal(name, params):
        this.parts.push(name);
        if (params.length > 0) {
          var delim = "<";
          for (p in params) {
            this.parts.push(delim);
            visit(p);
            delim = ", ";
          }
          this.parts.push(">");
        }
      case Wildcard:
        if (this.nextVar >= this.varNames.length)
          throw new PatternError("To many wildcards!");
        
        this.parts.push(this.varNames[this.nextVar++]);
    }
  }

  public function toString() {
    return parts.join("");
  }
}

// For converting a Type to a TypePattern
private class CreateCtx {
  private var typeParamsIdx: Map<String, Int>;
  private var unusedTypeParams: Map<String, Void>;
  public var paramList(default, null): Array<Int>;
  public var errors(default, null): Array<String>;

  public function new() {
    this.typeParamsIdx = new Map();
    this.unusedTypeParams = new Map();
    this.paramList = [];
    this.errors = [];
  }

  public inline function getUnusedParams(): Iterator<String> {
    return unusedTypeParams.keys();
  }

  public function addTypeParam(param: Type, idx: Int): Void {
    var inst = switch (param) {
      case TInst(ref, _): ref.get();
      default:
        addError('Invalid type parameter: ${param.toString()}');
        return;
    };

    var name = MacroUtils.typePathToString(inst);
    if (this.typeParamsIdx.exists(name))
      return addError('Duplicate type parameter: $name');

    this.typeParamsIdx.set(name, idx);

    switch (inst.kind) {
      case KTypeParameter([]):
        if (inst.params.length > 0)
          addError('Unsupported higher-kinded type parameter: ${param.toString()}');
        this.unusedTypeParams.set(name, null);

      case KTypeParameter(_): addError('Unsupported constrained type parameter: $name');
      default: addError('Invalid type parameter: $name');
    }
  }

  public function finish(): Void {
    for (unused in this.unusedTypeParams.keys()) {
      addError('Unused type parameter: $unused');
    }
  }

  public function createPattern(type: Type): TypePatternKind {
    return switch (type) {
      case TLazy(lazy): createPattern(lazy());
      case TMono(mono):
        var t = mono.get();
        if (t == null) {
          createError('Unexpected unresolved type: ${type.toString()}');
        } else {
          createPattern(t);
        }
      case TInst(ref, params):
        var inst = ref.get();
        switch (inst.kind) {
          case KNormal: createNominalPattern(inst, params);
          case KTypeParameter(_):createTypeParamPattern(inst);
          case kind: createError('Unsupported exotic class kind ${kind.getName()}: ${type.toString()}');
        }
      case TType(ref, params): createNominalPattern(ref.get(), params);
      case TEnum(ref, params): createNominalPattern(ref.get(), params);
      case TAbstract(ref, params): createNominalPattern(ref.get(), params);
      case TFun(_, _): createError('Unsupported function type: ${type.toString()}');
      case TAnonymous(_): createError('Unsupported anonymous type: ${type.toString()}');
      case TDynamic(_): createError('Unsupported dynamic type: ${type.toString()}');
    };
  }

  private function createNominalPattern(path: BaseType, params: Array<Type>): TypePatternKind {
    var params = params.map(this.createPattern);
    return Nominal(MacroUtils.typePathToString(path), params);
  }

  private function createTypeParamPattern(path: BaseType): TypePatternKind {
    var name = MacroUtils.typePathToString(path);
    var idx = this.typeParamsIdx.get(name);
    if (idx == null)
      return createError('Unknown type parameter: $name');
    if (!this.unusedTypeParams.exists(name))
      return createError('Unsupported repeated type parameter: $name');
    this.unusedTypeParams.remove(name);

    this.paramList.push(idx);
    return Wildcard;
  }

  private inline function createError(error: String): TypePatternKind {
    addError(error);
    return Wildcard;
  }

  private inline function addError(error: String): Void {
    this.errors.push(error);
  }
}
#end
