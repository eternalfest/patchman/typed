package typed.macro.pattern;

#if macro

class PatternError {
  private var msg: String;

  public function new(msg: String) {
    this.msg = msg;
  }

  public inline function toString(): String {
    return this.msg;
  }
}
#end
