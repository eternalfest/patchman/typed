package typed;

import ef.Error;
import patchman.ds.ReadOnlyArray;

class ReaderContext {
  
  private var messages: Array<ReaderDiagnostic>;
  private var messagesLen: Int;

  public function new(): Void {
    this.messages = [];
    this.messagesLen = 0;
  }

  public function error(msg: String): ReaderDiagnostic {
    var diag = new ReaderDiagnostic(msg, true);
    this.messages.push(diag);
    this.messagesLen++;
    return diag;
  }

  public function warn(msg: String): ReaderDiagnostic {
    var diag = new ReaderDiagnostic(msg, false);
    this.messages.push(diag);
    this.messagesLen++;
    return diag;
  }

  public inline function getMessages(): ReadOnlyArray<ReaderDiagnostic> {
    return this.messages;
  }

  public inline function isValid(): Bool {
    return this.isValidSince(0);
  }

  public inline function execIn(prop: ObjectProperty, func: Void -> Void): Void {
    var checkpoint = this.checkpoint();
    func();
    if (this.checkpoint() > checkpoint) {
      this.nestMessagesIn(prop, checkpoint);
    }
  }

  // "Raw" API

  public inline function checkpoint(): Int {
    return this.messagesLen;
  }

  public function nestMessagesIn(prop: ObjectProperty, checkpoint: Int): Void {
    for (i in checkpoint...this.messagesLen) {
      this.messages[i].nestIn(prop);
    }
  }

  public function isValidSince(checkpoint: Int): Bool {
    for (i in checkpoint...this.messagesLen) {
      if (this.messages[i].isError) {
        return false;
      }
    }
    return true;
  }
}
