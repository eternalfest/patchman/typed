package typed;

#if macro
import haxe.macro.Expr;
import typed.macro.TypedImpl;
#end

typedef TypedReader<T> = ReaderContext -> Dynamic -> Null<T>;

class Typed {

  public static function readWith<T>(reader: TypedReader<T>, obj: Dynamic): T {
    var ctx = new ReaderContext();
    var result = reader(ctx, obj);
    if (ctx.isValid()) {
      return result;
    } else {
      // TODO: proper error type
      throw ctx.getMessages();
    }
  }

  public static macro function createReader<T>(): ExprOf<TypedReader<T>> {
    return TypedImpl.getInstance().createReaderFromContext(true);
  }

  public static macro function read<T>(obj: ExprOf<Dynamic>): ExprOf<T> {
    var reader = TypedImpl.getInstance().createReaderFromContext(false);
    return macro typed.Typed.readWith($reader, $obj);
  }

  public static macro function showAllRegisteredReaders(): ExprOf<Void> {
    return TypedImpl.getInstance().showAllRegisteredReaders();
  }

}
