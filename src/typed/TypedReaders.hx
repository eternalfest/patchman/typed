package typed;

import typed.Typed;

class TypedReaders {

  /////////////////////
  // PRIMITIVE TYPES //
  /////////////////////

  @:typedReader
  public static function nullableOf<T>(elem: TypedReader<T>): TypedReader<Null<T>> {
    return function(ctx, obj) {
      return obj == null ? null : elem(ctx, obj);
    };
  }

  @:typedReader(singleton)
  public static function int(ctx: ReaderContext, obj: Dynamic): Null<Int> {
    if (TypeUtils.isInt(obj)) {
      return obj;
    } else {
      ctx.error(obj + " isn't a valid integer");
      return null;
    }
  }

  @:typedReader(singleton)
  public static function float(ctx: ReaderContext, obj: Dynamic): Null<Float> {
    if (TypeUtils.isFloat(obj)) {
      return obj;
    } else {
      ctx.error(obj + " isn't a valid float");
      return null;
    }
  }

  @:typedReader(singleton)
  public static function bool(ctx: ReaderContext, obj: Dynamic): Null<Bool> {
    if (TypeUtils.isBool(obj)) {
      return obj;
    } else {
      ctx.error(obj + " isn't a valid boolean");
      return null;
    }
  }

  @:typedReader(singleton)
  public static function string(ctx: ReaderContext, obj: Dynamic): Null<String> {
    if (TypeUtils.isString(obj)) {
      return obj;
    } else {
      ctx.error(obj + " isn't a valid string");
      return null;
    }
  }

  ////////////////////
  // COMPOUND TYPES //
  ////////////////////

  @:typedReader
  public static function arrayOf<T>(elem: TypedReader<T>): TypedReader<Array<T>> {
    return function(ctx, obj: Dynamic) {
      if (TypeUtils.isArray(obj)) {
        var len = obj.length;
        var result: Array<T> = new Array();
        var checkpoint = ctx.checkpoint();
        for (i in 0...len) {
          result.push(elem(ctx, obj[i]));
          if (ctx.checkpoint() > checkpoint) {
            ctx.nestMessagesIn(i, checkpoint);
            checkpoint = ctx.checkpoint();
          }
        }
        return result;
      } else {
        ctx.error(obj + " isn't a valid array");
        return null;
      }
    };
  }

  @:typedReader
  public static function recordOf<T>(value: TypedReader<T>): TypedReader<Map<String, T>> {
    return function(ctx, obj: Dynamic) {
      if (TypeUtils.isObject(obj)) {
        var result: Map<String, T> = new Map();
        var checkpoint = ctx.checkpoint();

        TypeUtils.forEachField(obj, function(field) {
          result.set(field, value(ctx, Reflect.field(obj, field)));
          if (ctx.checkpoint() > checkpoint) {
            ctx.nestMessagesIn(field, checkpoint);
            checkpoint = ctx.checkpoint();
          }
        });
        return result;
      } else {
        ctx.error(obj + " isn't a valid object");
        return null;
      }
    };
  }
}
