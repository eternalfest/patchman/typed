package typed;

class ReaderDiagnostic {
  
  public var message(default, null): String;
  public var isError(default, null): Bool;
  private var location: Array<ObjectProperty>;

  public function new(message: String, isError: Bool): Void {
    this.message = message;
    this.isError = isError;
    this.location = [];
  }

  public inline function nestIn(prop: ObjectProperty): Void {
    this.location.push(prop);
  }

  public function toString(): String {
    var parts = [this.isError ? "Error at $" : "Warning at $"];

    var i = this.location.length;
    while (i-- > 0) {
      var value = this.location[i];
      var idx = value.asIndex();
      if (idx != null) {
        parts.push("[");
        parts.push(Std.string(idx));
        parts.push("]");
      } else {
        parts.push(".");
        parts.push(value.asField());
      }
    }

    parts.push(": ");
    parts.push(this.message);
    return parts.join("");
  }
}
