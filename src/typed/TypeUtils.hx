package typed;


class TypeUtils {

  public static inline function isInt(obj: Dynamic): Bool {
    // TODO: optimize this for the flash8 target
    return Std.is(obj, Int);
  }

  public static inline function isFloat(obj: Dynamic): Bool {
    // TODO: optimize this for the flash8 target
    return Std.is(obj, Float);
  }

  public static inline function isBool(obj: Dynamic): Bool {
    // TODO: optimize this for the flash8 target
    return Std.is(obj, Bool);
  }

  public static inline function isString(obj: Dynamic): Bool {
    // TODO: optimize this for the flash8 target
    return Std.is(obj, String);
  }

  public static inline function isArray(obj: Dynamic): Bool {
    // TODO: optimize this for the flash8 target
    return Std.is(obj, Array);
  }

  public static inline function isObject(obj: Dynamic): Bool {
    // TODO: optimize this for the flash8 target
    return Reflect.isObject(obj) && !Std.is(obj, Array);
  }

  public static inline function forEachField(obj: Dynamic, f: String -> Void): Void {
    // TODO: optimize this for the flash8 target
    for (field in Reflect.fields(obj)) {
      f(field);
    }
  }
}
